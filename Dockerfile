FROM jekyll/builder
LABEL maintainer "Darragh Grealish darragh@56k.cloud at Certus LUX"
COPY ./ .
RUN bundle install
RUN bundle exec jekyll build -d public



